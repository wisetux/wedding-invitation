# P&D Wedding Invitation
This is a website created for our wedding invitation. You can see the website live here: https://wedding.aadya.tech

## Table of Contents
- [Features](#features)
- [Installation](#installation)
- [Usage](#usage)
- [Screenshots](#screenshots)
- [Contributing](#contributing)
- [Credits](#credits)
- [License](#license)

## Features
- Lower navigation bar using HTML, CSS and JavaScript.
- Smooth scrolling in each section.
- Developed first with the Mobile First methodology, then for desktop.
- Compatible with all mobile devices and with a beautiful and pleasant user interface.

## Installation
This website is written in pure HTML, CSS and JS. As such you can use most modern webservers and simply upload it to root directory.

Alternatively you can run this using docker by following steps:
- Use git to clone this project

		git clone https://gitlab.com/wisetux/wedding-website
- Dpeloy nginx container with path of the project mounted within docker

		docker run --name wedding-invitation -d -p 80:80 -v /home/wisetux/Downloads/wedding-invitation:/usr/share/nginx/html

## Usage
You can browse the site by navigating to http://localhost:80 or using your server IP address

## Screenshots
[Mobile 1](/preview/mobile1.png)
[Mobile 2](/preview/mobile2.png)
[Laptop 1](/preview/laptop1.png)
[Laptop 2](/preview/laptop2.png)

## Contributing
I'm not a programmer by trade and simply worked on this website as a break from my ongoing wedding preparations. As such expect bugs and so all contributions are welcome! Please open an issue or submit a pull request.

## Credits
- Marlon [bedimcode](https://github.com/bedimcode/responsive-bottom-navigation) whose repository this project is forked from.
- [W3 Schools](https://www.w3schools.com/css/) for their CSS tutorials

## License
This project is licensed under GPLv3.
